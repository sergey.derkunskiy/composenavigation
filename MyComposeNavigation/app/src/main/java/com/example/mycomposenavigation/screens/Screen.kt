package com.example.mycomposenavigation.screens

const val DETAIL_ARG_KEY = "id"
const val DETAIL_ARG_KEY2 = "name"

const val ROOT_GRAPH_ROUTE = "root"
const val AUTH_GRAPH_ROUTE = "auth"
const val HOME_GRAPH_ROUTE = "home"

sealed class Screen(val route: String){
    object Home: Screen(route = "home_screen")
    object Detail: Screen(route = "detail_screen/{$DETAIL_ARG_KEY}/{$DETAIL_ARG_KEY2}"){
//        fun passId(id: Int): String {
//            return this.route.replace("{$DETAIL_ARG_KEY}", id.toString())
//        }

        fun passNameAndId(id: Int, name: String): String {
            return "detail_screen/$id/$name"
        }
    }
    object Login: Screen(route = "login_screen")
    object SignUp: Screen(route = "sign_up_screen")
}
