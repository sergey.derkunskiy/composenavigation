package com.example.mycomposenavigation.nav_graph

import android.util.Log
import androidx.navigation.*
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.mycomposenavigation.DetailScreen
import com.example.mycomposenavigation.HomeScreen
import com.example.mycomposenavigation.screens.DETAIL_ARG_KEY
import com.example.mycomposenavigation.screens.DETAIL_ARG_KEY2
import com.example.mycomposenavigation.screens.HOME_GRAPH_ROUTE
import com.example.mycomposenavigation.screens.Screen

fun NavGraphBuilder.homeNavGraph(
    navController: NavHostController
){
    navigation(
        startDestination = Screen.Home.route,
        route = HOME_GRAPH_ROUTE
    ) {
        composable(
            route = Screen.Home.route
        ) {
            HomeScreen(navController = navController)
        }
        composable(
            route = Screen.Detail.route,
            arguments = listOf(
                navArgument(DETAIL_ARG_KEY){
                    type = NavType.IntType
                },
                navArgument(DETAIL_ARG_KEY2){
                    type = NavType.StringType
                }
            )
        ) {
            Log.e("Args", it.arguments?.getInt(DETAIL_ARG_KEY).toString())
            Log.e("Args2", it.arguments?.getString(DETAIL_ARG_KEY2).toString())
            DetailScreen(navController = navController)
        }
    }

}