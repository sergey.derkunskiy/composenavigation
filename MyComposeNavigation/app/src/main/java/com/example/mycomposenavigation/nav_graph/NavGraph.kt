package com.example.mycomposenavigation.nav_graph

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import androidx.navigation.navigation
import com.example.mycomposenavigation.DetailScreen
import com.example.mycomposenavigation.HomeScreen
import com.example.mycomposenavigation.screens.*

@Composable
fun SetupNavGraph(
    navController: NavHostController
) {
    NavHost(
        navController = navController,
        startDestination = HOME_GRAPH_ROUTE,
        route = ROOT_GRAPH_ROUTE
    )
    {
        homeNavGraph(navController = navController)
        authNavGraph(navController = navController)

    }
}